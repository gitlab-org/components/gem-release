# `gem-release` component

The component generates jobs that handle the release of a new gem version.

Upon a change to a specific pattern (defaults to `lib/**/version.rb`) in the default branch:

1. Release notes are computed from `Changelog:` Git trailers
1. Release (and tag) is created with these notes
1. Gem is built (native gems building is supported)
1. Gem is pushed to Rubygems

As long as the gem is located under the `gitlab-org/ruby/gems` group, there's no need to provide
a Rubygems.org API token (it's automatically set with the `GEM_HOST_API_KEY` CI/CD variable).

## Usage

To utilize this component, add it to an existing `.gitlab-ci.yml` file using the `include:` keyword:

```yaml
include:
  - component: gitlab.com/gitlab-org/components/gem-release/gem-release@main
  # or a specific version
  - component: gitlab.com/gitlab-org/components/gem-release/gem-release@<VERSION>
```

## Inputs

| Input                             | Default Value             | Description                                                   |
| --------------------------------- | ------------------------- | ------------------------------------------------------------- |
| `gem_name`                        | `$CI_PROJECT_NAME`        | The name of the gem to release.                               |
| `file_pattern_to_trigger_release` | `"lib/**/version.rb"`     | The file pattern which triggers a new release.                |
| `build_native_gems`               | `false`                   | Whether to build native gems or not.                          |
| `smoke_test_before_script`        | `gem install ${GEM_FILE}` | Custom `before_script` (e.g. to install a native gem).        |
| `smoke_test_script`               | `gem info ${GEM_FILE}`    | Custom `script` to run a smoke test on the built gem.         |
| `dry_run`                         | `false`                   | Performs a dry-run (don't create a release nor push gems).    |


### Example

```yaml
include:
  - component: "gitlab.com/gitlab-org/components/gem-release/gem-release@1.0.0"
    inputs:
      smoke_test_script: "ruby -r 'gitlab-dangerfiles' -e \"puts Gitlab::Dangerfiles::VERSION\""
```

## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components 
